//
//  Constant.swift
//  Constant
//
//  Created by VAMSHI K on 10/4/23.
//

import UIKit
import Foundation

struct APIManager {
    static let baseURL = "https://data.cityofnewyork.us/resource/"
    static let schoolListAPI = "s3k6-pzi2.json"
    static let schoolDetailAPI = "f9bf-2cp4.json"
}

struct APIMethod{
    static let GET = "GET"
}

struct ContentType{
    static let kAppJson = "application/json"
    static let kContentType = "Content-Type"
}

struct AlertText {
    static let appName = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
    static let OkButton = "OK"
    static let noInternet = "No Internet Connection Available."
}

struct CellIdentifier {
    static let kListTvCell = "NSListTvCell"
}

struct ControllerName {
    static let kNSSchoolDetailsVC = "NSSchoolDetailsVC"
}

struct StoryboardName{
    static let kMainSB = UIStoryboard(name: "Main", bundle:nil)
}

struct ApiError {
    static let kApiError = "API Error"
}

struct ProgressbarColor {
    static let PrimaryColor = UIColor(red: 19.0/255.0, green: 155.0/255.0, blue: 255.0/255.0, alpha: 1.0)
}


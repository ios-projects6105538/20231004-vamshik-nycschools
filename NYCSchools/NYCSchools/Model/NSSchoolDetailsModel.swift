//
//  NSSchoolDetailsModel.swift
//  NYCSchools
//
//  Created by VAMSHI K on 10/4/23.
//

import Foundation

struct NSSchoolDetailsModel : Codable {
    let dbn : String?
    let num_of_sat_test_takers : String?
    let sat_critical_reading_avg_score : String?
    let sat_math_avg_score : String?
    let sat_writing_avg_score : String?
    let school_name : String?
}

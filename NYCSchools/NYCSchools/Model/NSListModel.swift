//
//  NSListModel.swift
//  NYCSchools
//
//  Created by VAMSHI K on 10/4/23.
//

import Foundation

struct NSSchoolListModel : Codable {
    let academicopportunities1 : String?
    let academicopportunities2 : String?
    let admissionspriority11 : String?
    let admissionspriority21 : String?
    let admissionspriority31 : String?
    let attendance_rate : String?
    let bbl : String?
    let bin : String?
    let boro : String?
    let borough : String?
    let building_code : String?
    let bus : String?
    let census_tract : String?
    let city : String?
    let code1 : String?
    let community_board : String?
    let council_district : String?
    let dbn : String?
    let directions1 : String?
    let ell_programs : String?
    let extracurricular_activities : String?
    let fax_number : String?
    let finalgrades : String?
    let grade9geapplicants1 : String?
    let grade9geapplicantsperseat1 : String?
    let grade9gefilledflag1 : String?
    let grade9swdapplicants1 : String?
    let grade9swdapplicantsperseat1 : String?
    let grade9swdfilledflag1 : String?
    let grades2018 : String?
    let interest1 : String?
    let latitude : String?
    let location : String?
    let longitude : String?
    let method1 : String?
    let neighborhood : String?
    let nta : String?
    let offer_rate1 : String?
    let overview_paragraph : String?
    let pct_stu_enough_letiety : String?
    let pct_stu_safe : String?
    let phone_number : String?
    let primary_address_line_1 : String?
    let program1 : String?
    let requirement1_1 : String?
    let requirement2_1 : String?
    let requirement3_1 : String?
    let requirement4_1 : String?
    let requirement5_1 : String?
    let school_10th_seats : String?
    let school_accessibility_description : String?
    let school_email : String?
    let school_name : String?
    let school_sports : String?
    let seats101 : String?
    let seats9ge1 : String?
    let seats9swd1 : String?
    let state_code : String?
    let subway : String?
    let total_students : String?
    let website : String?
    let zip : String?
}

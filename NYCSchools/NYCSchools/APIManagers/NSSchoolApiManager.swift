//
//  NSSchoolApiManager.swift
//  NYCSchools
//
//  Created by VAMSHI K on 10/4/23.


import UIKit
import Foundation

class NSSchoolApiManager {
    
    let HTTPMethod = APIMethod()
    let baseAPICall = NSBaseAPIManager()
    
    func schoolListApiManager(fromVC:UIViewController,  completion: @escaping (Result<[NSSchoolListModel], Error>) -> ()) {
        baseAPICall.request(urlString: APIManager.schoolListAPI, fromVC:fromVC ,method: APIMethod.GET) { (_ result: Result<[NSSchoolListModel], Error>)  in
            switch result {
            case .success(let schoolList):
                completion(.success(schoolList))
            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
    
    func schoolDetailsManager(fromVC:UIViewController,  completion: @escaping (Result<[NSSchoolDetailsModel], Error>) -> ()) {
        baseAPICall.request(urlString: APIManager.schoolDetailAPI, fromVC:fromVC ,method: APIMethod.GET) { (_ result: Result<[NSSchoolDetailsModel], Error>)  in
            switch result {
            case .success(let schoolDetails):
                completion(.success(schoolDetails))
            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
}

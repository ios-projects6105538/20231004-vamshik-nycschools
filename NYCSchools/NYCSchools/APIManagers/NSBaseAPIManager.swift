//
//  NSBaseAPIManager.swift
//  NYCSchools
//
//  Created by VAMSHI K on 10/4/23.


import Foundation
import UIKit

class NSBaseAPIManager{
    let HTTPMethod = APIMethod()
    let contentType = ContentType()
    
    func request<T:Codable>(urlString:String, fromVC:UIViewController, method:String, completion: @escaping((Result<T,Error>)->Void)){
        
        if !self.checkIsReachable(){
            fromVC.displayAlert(message: AlertText.noInternet)
            return
        }
        guard let url = URL(string: (APIManager.baseURL + urlString)) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = method
        request.setValue(ContentType.kAppJson, forHTTPHeaderField: ContentType.kContentType)
        callWithDic(with: request, urlString: urlString, completion: completion)
    }
    
    func callWithDic<T:Codable>(with request:URLRequest, urlString:String, completion: @escaping((Result<T,Error>)->Void)){
        URLSession.shared.dataTask(with: request) { (data, resp, err) in
            if let err = err {
                completion(.failure(err))
                return
            }
            guard let data = data else{
                completion(.failure(err!));return
            }
            do {
                let object = try JSONDecoder().decode(T.self, from: data)
                completion(.success(object))
            } catch let jsonError {
                completion(.failure(jsonError))
            }
        }.resume()
    }
    
    
    //MARK:- Network Methods
    func checkIsReachable()->Bool{
        var isReachable = true
        AppManager.isUnreachable { manager in
            print("Network is Unavailable")
            isReachable = false
        }
        return isReachable
    }
}




//
//  Extension.swift
// Extension
//
//  Created by VAMSHI K on 10/4/23.
//

import UIKit

extension UIViewController {
    
    func displayAlert(message : String,onDismissBack:Bool = false,root:Bool = false){
        let alert = UIAlertController(title: AlertText.appName, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: AlertText.OkButton, style: .default, handler: {_ in
            if onDismissBack{
                if root{
                    self.navigationController?.popToRootViewController(animated: true)
                }else{
                    self.navigationController?.popViewController(animated: true)}
            }
        })
        alert.addAction(ok)
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
}

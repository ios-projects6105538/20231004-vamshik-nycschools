//
//  NSSchoolListViewModel.swift
//  NYCSchools
//
//  Created by VAMSHI K on 10/4/23.

import UIKit
import Foundation

class NSSchoolListViewModel {
    let schoolListApiManager = NSSchoolApiManager()
    
    func schoolListApi(fromVC:UIViewController,  completion: @escaping (Result<[NSSchoolListModel], Error>) -> ()) {
        schoolListApiManager.schoolListApiManager(fromVC: fromVC) { (result)  in
            switch result {
            case .success(let schoolList):
                completion(.success(schoolList))
            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
}

//
//  NSSchoolDetailViewModel.swift
//  NYCSchools
//
//  Created by VAMSHI K on 10/4/23.


import UIKit
import Foundation

class NSSchoolDetailViewModel {
    let schoolListApiManager = NSSchoolApiManager()
    
    func schoolDetailtApi(fromVC:UIViewController,  completion: @escaping (Result<[NSSchoolDetailsModel], Error>) -> ()) {
        schoolListApiManager.schoolDetailsManager(fromVC: fromVC) { (result)  in
            switch result {
            case .success(let detailsModel):
                completion(.success(detailsModel))
            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
}

//
//  AppManager.swift
//   NYCSchools
//
//  Created by VAMSHI K on 10/4/23.


import UIKit
import Foundation

class AppManager: NSObject{
    
    var reachability: Reachability!
    static let sharedInstance: AppManager = {
        return AppManager()
    }()
    
    override init() {
        super.init()
        // Initialise reachability
        reachability = Reachability()!
        // Register an observer for the network status
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        do {
            // Start the network status notifier
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        // Do something globally here!
        print("status changed")
        let reachabilitys = notification.object as! Reachability
        if reachabilitys.connection != .none {
            if reachabilitys.connection == .wifi {
                print("Reachable via WiFi")
                
            } else {
                print("Reachable via Cellular")}
            
        } else {
            print("Not reachable")
            
        }
    }
    static func stopNotifier() -> Void {
        print("stop noti")
        (AppManager.sharedInstance.reachability).stopNotifier()
    }
    
    // Network is reachable
    static func isReachable(completed: @escaping (AppManager) -> Void) {
        if (AppManager.sharedInstance.reachability).connection != .none {
            completed(AppManager.sharedInstance)
        }
    }
    
    // Network is unreachable
    static func isUnreachable(completed: @escaping (AppManager) -> Void) {
        if (AppManager.sharedInstance.reachability).connection == .none {
            completed(AppManager.sharedInstance)
        }
    }
    
    // Network is reachable via WWAN/Cellular
    static func isReachableViaWWAN(completed: @escaping (AppManager) -> Void) {
        if (AppManager.sharedInstance.reachability).connection == .cellular {
            completed(AppManager.sharedInstance)
        }
    }
    
    // Network is reachable via WiFi
    static func isReachableViaWiFi(completed: @escaping (AppManager) -> Void) {
        if (AppManager.sharedInstance.reachability).connection == .wifi {
            completed(AppManager.sharedInstance)
        }
    }
    
    deinit {
        print("deinit")
        reachability?.stopNotifier()
        
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
    }
}

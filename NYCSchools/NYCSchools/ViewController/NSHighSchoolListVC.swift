//
//  NSHighSchoolListVC.swift
//  NYCSchools
//
//  Created by VAMSHI K on 10/4/23.
//

import UIKit

class NSHighSchoolListVC: UIViewController {
    
    // MARK:- All IBOutlet and property
    @IBOutlet weak var viewNavigation : UIView!
    @IBOutlet weak var tblView: UITableView!
    var schoolListModel : [NSSchoolListModel] = []
    let schoolListViewModel = NSSchoolListViewModel()
    
    var indicator = UIActivityIndicatorView()
    
    // MARK:- View Controller Cycle
    override func viewDidLoad() {
        self.setUpVC()
        super.viewDidLoad()
        // Start loading activity Indicator
        activityIndicator()
        indicator.startAnimating()
        self.callWebService()
    }
    
    // MARK:- UI Activity Indicator View
    func activityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.style = UIActivityIndicatorView.Style.large
        indicator.center = self.view.center
        indicator.backgroundColor = .white
        self.view.addSubview(indicator)
    }
    
        // MARK:- Setup View
    func setUpVC(){
        self.tblView.register(UINib(nibName: CellIdentifier.kListTvCell, bundle: nil), forCellReuseIdentifier: CellIdentifier.kListTvCell)
    }
    
    // MARK:- Web Service Call
    func callWebService(){
        schoolListViewModel.schoolListApi(fromVC:self) { [weak self] (resultInfo) in
            switch resultInfo {
            case .success(let resultInfo):
                self?.schoolListModel = resultInfo
                DispatchQueue.main.async(execute: {
                    self?.tblView.reloadData()
                    // stop loading Activity Indicator
                    self?.indicator.stopAnimating()
                    self?.indicator.hidesWhenStopped = true
                })
            case .failure( _):
                self?.displayAlert(message: ApiError.kApiError)
            }
        }
    }
}

// MARK:- UITableView Delegate and Data Source
extension NSHighSchoolListVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schoolListModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.kListTvCell, for: indexPath) as? NSListTvCell
        guard let cell = cell else { return UITableViewCell() }
        cell.configureCell(item: schoolListModel[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schoolDetailsVC = StoryboardName.kMainSB.instantiateViewController(withIdentifier: ControllerName.kNSSchoolDetailsVC) as? NSSchoolDetailsVC
        guard let schoolDetailsVC = schoolDetailsVC else { return }
        schoolDetailsVC.schoolListModel    = schoolListModel[indexPath.row]
        self.navigationController?.pushViewController(schoolDetailsVC, animated: true)
    }
}

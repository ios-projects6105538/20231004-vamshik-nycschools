//
//  NSListTvCell.swift
//  NYCSchools
//
//  Created by VAMSHI K on 10/4/23.
//

import UIKit

class NSListTvCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    //Sets the selected state of the cell, optionally animating the transition between states.
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(item: NSSchoolListModel){
        self.lblName.text       = (item.school_name ?? "")
        self.lblMobile.text     = (item.phone_number ?? "")
        self.lblEmail.text      = (item.school_email ?? "")
        
        let add1 = (item.primary_address_line_1 ?? "") + ", "
        let add2 = (item.state_code ?? "") + " - " + (item.zip ?? "")
        self.lblAddress.text    =  add1 + add2
    }
}

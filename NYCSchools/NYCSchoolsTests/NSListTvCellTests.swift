//
//  NSListTvCellTests.swift
//  NYCSchoolsTests
//
//  Created by VAMSHI K on 10/4/23.
//


import XCTest
import Foundation

@testable import NYCSchools

class NSListTvCellTests: XCTestCase {
    var listTvCell = NSListTvCell()
    var schoolListModel : [NSSchoolListModel]?

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAwakeFromNib(){
        listTvCell.awakeFromNib()
    }
    
    func testSetSelected(){
        listTvCell.setSelected(true, animated: true)
    }
}

//
//  NSHighSchoolListTests.swift
//  NYCSchoolsTests
//
//  Created by VAMSHI K on 10/4/23.
//

import XCTest
import Foundation

@testable import NYCSchools

class NSHighSchoolListTests: XCTestCase {
    var controller : NSHighSchoolListVC!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        controller = storyboard.instantiateViewController(withIdentifier: "NSHighSchoolListVC") as? NSHighSchoolListVC
        let viewController = controller.view
        XCTAssertNotNil(viewController)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNumberOfRow(){
        let numberOfRow = controller?.tableView(UITableView(), numberOfRowsInSection: 0) ?? 0
        XCTAssertNotNil(numberOfRow)
    }
    
    func testSchoolListMockJsonMethods(){
        let bundle = Bundle(for: type(of: self))
        guard let fileUrl = bundle.url(forResource: "NYCSchoolsTestsJson", withExtension: "json") else { return }
        guard let data = try? Data(contentsOf: fileUrl) else { return }
        do {
            let schoolListViewModel = try JSONDecoder().decode(NSSchoolListModel.self, from: data)
            XCTAssertNotNil(schoolListViewModel)
        } catch {
            print("Model Mapping failed with error: \(error)")
        }
    }
}


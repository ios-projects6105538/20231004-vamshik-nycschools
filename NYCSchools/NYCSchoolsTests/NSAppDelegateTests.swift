//
//  AppDelegateTestCases.swift
//  NYCSchoolsTests
//
//  Created by VAMSHI K on 10/4/23.
//

import XCTest

@testable import NYCSchools

class NSAppDelegateTests: XCTestCase {
    
    var appDelegate = AppDelegate()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        appDelegate = UIApplication.shared.delegate as! AppDelegate
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testApplicationWillResignActive(){
        XCTAssertNotNil(appDelegate.applicationWillResignActive(UIApplication.shared), "View Will Resign Active")
    }
    
    func testApplicationDidEnterBackground()  {
        XCTAssertNotNil(appDelegate.applicationDidEnterBackground(UIApplication.shared), "View Did Enter Background")
    }
    
    func testApplicationWillEnterForeground() {
        XCTAssertNotNil(appDelegate.applicationWillEnterForeground(UIApplication.shared), "View Did Enter Forground")
    }
    
    func testApplicationDidBecomeActive() {
        XCTAssertNotNil(appDelegate.applicationDidBecomeActive(UIApplication.shared), "View Did Become Active")
    }
    
    func testApplicationWillTerminate(){
        XCTAssertNotNil(appDelegate.applicationWillTerminate(UIApplication.shared), "View will terminate")
    }
}


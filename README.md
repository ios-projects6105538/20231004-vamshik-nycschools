# 20231004-VamshiK-NYCSchools

# Project : NYC High Schools (code Challenge)
# App Name : NYCSchools

support :
 Xcode 15.0 and above
 iOS 13.0 and above
 Swift 5.4 and above
 All iPhone Devices
 Orientation : Portrait

Project Structure :
1) View Controller Class
2) ViewModel
3) Model Class
2) Storyboard
3) Network Manager Class
4) Constant File
7) Unit Test Cases Class 
8) Reachability 
9) Documentation Files (ReadMe, Screenshots)

Project :
-> The project developed using native iOS Frameworks Swift and UIkit 

